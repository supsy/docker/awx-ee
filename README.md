# AWX-EE

AWX Custom Execution Environment

## Installation

```bash
virtualenv -ppython3 builder
```

## Generate
```bash
ansible-builder build -v 3 --tag awx-custom-ee:latest --container-runtime docker --context ansible-3.0/context -f ansible-3.0/execution-environment.yml

```
